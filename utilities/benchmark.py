import os
import time
import math
import random
from influxdb import InfluxDBClient
from git import Repo
import sys
import time
import subprocess
import shlex
import re

class MeasureMESAPD:
    def __init__(self, availableCores, buildFolder):
        self.availableCores = availableCores
        self.buildFolder = buildFolder
        self.benchmarkFolder = os.path.join(buildFolder, 'apps', 'benchmarks', 'GranularGas') 

    def measure(self):
        json_body = self.measureKernel() + self.measureGranularGas()
        return json_body

    def measureKernel(self):
        json_body = []

        numCores = min(8, self.availableCores)

        kernelbenchmark_exec = os.path.join(self.benchmarkFolder, 'MESA_PD_KernelBenchmark')
        kernelbenchmark_config = os.path.join(self.benchmarkFolder, 'MESA_PD_Benchmark.cfg')
        kernelbenchmark_cmd = 'mpirun -np '+str(numCores)+' '+kernelbenchmark_exec+' '+kernelbenchmark_config

        print(kernelbenchmark_cmd)
        start = time.time()
        process = subprocess.run(shlex.split(kernelbenchmark_cmd), 
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, 
                                 universal_newlines=True)
        end = time.time()
        run_time = end-start

        if process.returncode != 0:
            print(process.stdout)
            print(process.stderr)
        else:
            # tts = dict()
            # for s in process.stdout.splitlines():
            #     m = re.search(r'\[0\]\s*(\w*)\s*\|[\s\d\.\%]*\|\s*([\d\.]*)', s)

            #     if m is not None:
            #         tts[m.group(1)] = float(m.group(2))
            # #print(tts)

            # json_body += [
            #     {
            #         'measurement': 'mesa_pd_benchmark',
            #         'tags': {
            #             'host': os.uname()[1],
            #             'benchmark': 'kernel'
            #         },
            #         'fields': tts
            #     }
            # ]

            json_body += [
                {
                    'measurement': 'run_time',
                    'tags': {
                        'benchmark': 'MESA_PD_KernelBenchmark'
                    },
                    'fields': {
                        'run_time': run_time,
                        'num_cores': numCores
                    }
                }
            ]

        return json_body

    def measureGranularGas(self):
        json_body = []

        numCores = min(8, self.availableCores)

        granulargas_exec = os.path.join(self.benchmarkFolder, 'MESA_PD_GranularGas')
        granulargas_config = os.path.join(self.benchmarkFolder, 'GranularGas.cfg')
        granulargas_cmd = 'mpirun -np '+str(numCores)+' '+granulargas_exec+' '+granulargas_config
        print(granulargas_cmd)

        start = time.time()
        process = subprocess.run(shlex.split(granulargas_cmd), 
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, 
                                 universal_newlines=True)
        end = time.time()
        run_time = end-start

        if process.returncode != 0:
            print(process.stdout)
            print(process.stderr)
        else:
            json_body += [
                {
                    'measurement': 'run_time',
                    'tags': {
                        'benchmark': 'MESA_PD_GranularGas'
                    },
                    'fields': {
                        'run_time': run_time,
                        'num_cores': numCores
                    }
                }
            ]

        return json_body

def main():
    #    write_user_pw = 'HCFusn9joVAt3C'
    try:
        write_user_pw = os.environ["CUSTOM_ENV_INFLUXDB_WRITE_USER_PASSWORD"]
    except KeyError:
        try:
            write_user_pw = os.environ["INFLUXDB_WRITE_USER_PASSWORD"]
        except KeyError:
            print('Password for the InfluxDB write_user was not set.\n'
                 'See https://docs.gitlab.com/ee/ci/variables/#secret-variables', file=sys.stderr)
            exc_info = sys.exc_info()
            raise exc_info[0].with_traceback(exc_info[1], exc_info[2])

    if len(sys.argv) != 3:
        print("Usage: " + sys.argv[0] + " num_cores build_folder")
        exit(1)

    numCores = int(sys.argv[1])    
    buildFolder = sys.argv[2]

    point_time = int(time.time())
    host = os.uname()[1]

    print("Using "+str(numCores)+" tasks on "+host+" for benchmarking, timestamp: "+str(point_time)+".")

    json_body = []

    # perform measurements
    measureMesaPD = MeasureMESAPD(numCores, buildFolder)
    json_body += measureMesaPD.measure()

    # some annotations
    repo = Repo(search_parent_directories=True)
    commit = repo.head.commit

    for measurement in json_body:
        measurement['time'] = point_time
        measurement['fields']['commit'] = commit.hexsha
        measurement['tags']['host'] = host
    
    #print(json_body)

    # upload to influx db
    client = InfluxDBClient('i10web.informatik.uni-erlangen.de', 8086,
                            'benchmarks', write_user_pw, 'benchmarks')
    client.write_points(json_body, time_precision='s')


if __name__ == "__main__":
    main()
